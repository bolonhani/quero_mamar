from django.db import models
from base.models import Base
from ckeditor.fields import RichTextField


class News(Base):
    title = models.CharField(max_length=100, blank=True, null=True)
    short_text = models.CharField(max_length=300, blank=True, null=True)
    text = RichTextField(blank=True, null=True)

    @staticmethod
    def get_all():
        items = News.objects.all()

        return items

    def __str__(self):
        return self.title.encode("utf-8")

    class Meta:
        ordering = ['-created_at']
        verbose_name = "Blog"
