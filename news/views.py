# - * - encoding: utf-8 - * -
import uuid
from django.shortcuts import render
from news.models import News


def index(request):
    items = News.get_all()
    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-service.png',
        'items': items
    }
    return render(request, "news/index.html", context)


def detail(request, id):
    item = News.objects.get(pk=id)
    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-service.png',
        'item': item
    }
    return render(request, "news/detail.html", context)
