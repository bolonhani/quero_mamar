# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0014_auto_20160129_0153'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('base_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='base.Base')),
                ('title', models.CharField(max_length=200, null=True, blank=True)),
                ('text', models.TextField(null=True, blank=True)),
            ],
            bases=('base.base',),
        ),
    ]
