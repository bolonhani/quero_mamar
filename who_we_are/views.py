# - * - encoding: utf-8 - * -
import uuid
from django.shortcuts import render
from base.models import Content


def index(request):
    try:
        content = Content.objects.get(identifier='who-we-are')
    except Content.DoesNotExist:
        content = dict(
            title='Em breve',
            content_text='Estamos trabalhando para ter um melhor conteúdo para você :)'
        )

    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-who-we-are.png',
        'content': content
    }
    return render(request, "who_we_are/index.html", context)
