# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('who_we_are', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='whoweare',
            name='base_ptr',
        ),
        migrations.DeleteModel(
            name='WhoWeAre',
        ),
    ]
