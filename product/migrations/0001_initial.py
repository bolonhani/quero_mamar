# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0014_auto_20160129_0153'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('base_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='base.Base')),
                ('name', models.CharField(max_length=100, blank=True)),
                ('description', models.TextField(max_length=500, blank=True)),
                ('value', models.CharField(max_length=10, blank=True)),
                ('pagseguro', models.TextField(max_length=500, blank=True)),
            ],
            bases=('base.base',),
        ),
    ]
