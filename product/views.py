import uuid
from django.shortcuts import render
from .models import Product


def detail(request, id):
    item = Product.objects.get(pk=id)

    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-service.png',
        'item': item
    }

    return render(request, "product/detail.html", context)
