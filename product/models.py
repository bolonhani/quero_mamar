from django.db import models
from base.models import Base


class Product(Base):
    name = models.CharField(max_length=100, blank=True)
    description = models.TextField(max_length=500, blank=True)
    value = models.CharField(max_length=10, blank=True)
    pagseguro = models.TextField(max_length=600, blank=True)
    image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.name
