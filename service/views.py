# - * - encoding: utf-8 - * -
import uuid
from django.shortcuts import render
from service.models import Service


def index(request):
    items = Service.get_all()
    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-service.png',
        'items': items
    }
    return render(request, "service/index.html", context)


def detail(request, id):
    item = Service.objects.get(pk=id)
    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-service.png',
        'item': item
    }
    return render(request, "service/detail.html", context)
