# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0012_auto_20160126_0041'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('base_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='base.Base')),
                ('name', models.CharField(max_length=75, blank=True)),
                ('description', models.CharField(max_length=200, blank=True)),
                ('number', models.CharField(max_length=2, blank=True)),
            ],
            bases=('base.base',),
        ),
    ]
