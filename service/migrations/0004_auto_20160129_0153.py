# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0003_auto_20160128_2229'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='service',
            options={'ordering': ['number']},
        ),
    ]
