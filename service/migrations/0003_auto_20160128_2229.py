# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_auto_20160126_0134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='description',
            field=models.TextField(max_length=2000, verbose_name=b'Descri\xc3\xa7\xc3\xa3o', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(max_length=75, verbose_name=b'T\xc3\xadtulo', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='number',
            field=models.CharField(max_length=2, verbose_name=b'N\xc3\xbamero', blank=True),
        ),
    ]
