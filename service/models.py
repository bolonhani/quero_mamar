# - * - encoding: utf-8 - * -
from django.db import models
from base.models import Base
from ckeditor_uploader.fields import RichTextUploadingField


class Service(Base):
    name = models.CharField(max_length=75, blank=True, verbose_name='Título')
    description = RichTextUploadingField(max_length=2000, blank=True, verbose_name='Descrição')
    number = models.CharField(max_length=2, blank=True, verbose_name='Número')
    short_description = ''
    before_mod = 0
    mod = 0

    @staticmethod
    def get_all():
        items = Service.objects.all()

        for item in items:
            item.set_short_description()
            item.mod = int(eval('{0} % {1}'.format(item.number, 3)))
            item.before_mod = item.mod - 1

        return items

    def set_short_description(self):
        encoded_string = self.description.encode('utf-8')
        limited_string = encoded_string[:100]
        definitive_short_description = '{0} {1}'.format(limited_string, '[...]')

        self.short_description = definitive_short_description

    def __str__(self):
        return self.name.encode('utf-8')

    class Meta:
        ordering = ['number']
