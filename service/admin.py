# - * - encoding: utf-8 - * -
from django.contrib import admin
from service.models import Service


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('number', 'name')

admin.site.register(Service, ServiceAdmin)
