import uuid
from django.shortcuts import render

from news.models import News
from product.models import Product


def index(request):
    news = News.objects.all()[:3]
    product = Product.objects.all()[0]
    return render(request, "index/index.html", {'clean_cache': uuid.uuid1().hex, 'background_image': '/static/images/banner-home.png', 'news': news, 'product': product})


def construction(request):
    return render(request, "index/construction.html")
