# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('base_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='base.Base')),
                ('title', models.CharField(max_length=100, null=True)),
                ('content', models.TextField(max_length=500, null=True)),
            ],
            bases=('base.base',),
        ),
    ]
