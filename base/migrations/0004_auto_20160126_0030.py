# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20160126_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='content',
            field=models.TextField(default=b'conte\xc3\xbado', max_length=500, blank=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='title',
            field=models.CharField(default=b't\xc3\xadtulo', max_length=100, blank=True),
        ),
    ]
