# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0012_auto_20160126_0041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='base',
            name='active',
            field=models.BooleanField(default=True, verbose_name=b'Ativo'),
        ),
        migrations.AlterField(
            model_name='base',
            name='created_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Criado em'),
        ),
        migrations.AlterField(
            model_name='base',
            name='owner',
            field=models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='base',
            name='updated_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Atualizado em'),
        ),
        migrations.AlterField(
            model_name='content',
            name='content_text',
            field=models.TextField(default=b'Conteudo', max_length=2000),
        ),
    ]
