# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0007_auto_20160126_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='content',
            field=models.CharField(default=None, max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='title',
            field=models.CharField(default=None, max_length=100, null=True, blank=True),
        ),
    ]
