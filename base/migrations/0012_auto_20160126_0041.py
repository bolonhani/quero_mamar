# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0011_auto_20160126_0036'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='identifier',
            field=models.CharField(default=b'Identificador', help_text=b'N\xc3\xa3o altere este campo.', max_length=50),
        ),
        migrations.AlterField(
            model_name='content',
            name='content_text',
            field=models.TextField(default=b'Conteudo', max_length=500),
        ),
    ]
