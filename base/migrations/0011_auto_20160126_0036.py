# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0010_auto_20160126_0034'),
    ]

    operations = [
        migrations.RenameField(
            model_name='content',
            old_name='content',
            new_name='content_text',
        ),
    ]
