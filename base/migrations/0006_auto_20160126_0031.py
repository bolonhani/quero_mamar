# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_auto_20160126_0031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='content',
            field=models.TextField(default=None, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='title',
            field=models.CharField(default=None, max_length=100, null=True),
        ),
    ]
