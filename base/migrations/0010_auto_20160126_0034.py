# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0009_auto_20160126_0033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='content',
            field=models.CharField(default=b'Conteudo', max_length=500),
        ),
        migrations.AlterField(
            model_name='content',
            name='title',
            field=models.CharField(default=b'Titulo', max_length=100),
        ),
    ]
