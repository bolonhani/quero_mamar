# - * - encoding: utf-8 - * -
from django.contrib.auth.models import User
from django.db import models


class Base(models.Model):
    created_at = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name='Criado em')
    updated_at = models.DateTimeField(auto_now=False, auto_now_add=True, verbose_name='Atualizado em')
    active = models.BooleanField(default=True, verbose_name='Ativo')
    owner = models.ForeignKey(User)

    class Meta:
        abstract = False


class Content(Base):
    title = models.CharField(max_length=100, default='Titulo')
    content_text = models.TextField(max_length=2000, default='Conteudo')
    identifier = models.CharField(max_length=50, default='Identificador', help_text='Não altere este campo.')

    def __str__(self):
        return self.title
