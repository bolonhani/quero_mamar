from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=50)
    telephone = models.CharField(max_length=15)
    email = models.CharField(max_length=100)
    message = models.TextField()

    def __str__(self):
        return self.name.encode('utf-8')

    @staticmethod
    def create(**kwargs):
        contact = Contact()
        for key, value in kwargs.iteritems():
            if hasattr(contact, key):
                setattr(contact, key, value)

        contact.save()
        return contact

    def was_saved(self):
        try:
            Contact.objects.get(pk=self.id)
        except Contact.DoesNotExist:
            return False
        return True

    class Meta:
        db_table = "contact"
