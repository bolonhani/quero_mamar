import uuid
from django.shortcuts import render

from contact.models import Contact


def index(request):
    context = {
        'clean_cache': uuid.uuid1().hex,
        'background_image': '/static/images/banner-contact.png'
    }
    if request.POST:
        contact = Contact.create(**request.POST)
        if contact.was_saved():
            context.update({
                'class': 'success',
                'message': 'Sua mensagem foi enviada. Em breve entraremos em contato.'
            })
        else:
            context.update({
                'class': 'error',
                'message': 'Ops, houve um erro ao enviar sua mensagem. Tente novamente mais tarde.'
            })
    return render(request, "contato/index.html", context)
