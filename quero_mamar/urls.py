"""quero_mamar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url, patterns
from django.conf.urls.static import static
from django.contrib import admin

import index.views
import who_we_are.views
import service.views
import news.views
import contact.views
import product.views

urlpatterns = patterns(
    '',
    url(r'^$', index.views.construction, name='construction'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^site$', index.views.index, name='index'),
    url('site/quem-somos', who_we_are.views.index, name='quem-somos'),
    url('site/servicos', service.views.index, name='servicos'),
    url(r'^site/servico/(?P<id>\d+)', service.views.detail, name='servico'),
    url(r'^site/blog$', news.views.index, name='noticias'),
    url(r'^site/blog/(?P<id>\d+)$', news.views.detail, name='noticia'),
    url('site/contato', contact.views.index, name='contato'),
    url(r'^site/produto/(?P<id>\d+)$', product.views.detail, name='product'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls'))
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
